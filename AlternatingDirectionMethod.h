#pragma once

#include "Common.h"
#include "Instance.h"
#include "MilpModel.h"

//Gurobi status
//https://www.gurobi.com/documentation/9.0/refman/optimization_status_codes.html#sec:StatusCodes

class AlternatingDirectionMethod {

	//ATTRIBUTES

	//Computational parameters
	bool useCuts;
	bool useOldVI;
	bool viInFirstMILP;

	//Data + Solve
	Instance* instance;
	MilpModel* milpModel;

	//Percentage
	double ADMPercentage;

	//Time
	double ADMTimeLimit;
	double givenTimeLimit;
	chrono::time_point<chrono::system_clock> startTime;
	chrono::time_point<chrono::system_clock> ADMStartTime;
	double realADMTime;
	double improvementPhaseTime;

	//Solution
	vector<unordered_map<int, double>> bestXSolutionADM;
	vector<unordered_map<size_t, bool>> bestYSolutionADM;
	vector<unordered_map<int, double>> bestXSolution;
	vector<unordered_map<size_t, bool>> bestYSolution;
	double admObjectiveValue;
	double bestObjectiveValue;
	double bestObjectiveBound;
	bool enteredImprovement;

	//roundNumber function for statistics
	double roundNumber(double value);


public:

	//CONSTRUCTORS AND DESTRUCTORS
	AlternatingDirectionMethod(Instance* instance, double ADMTimeLimit, double ADMPercentage, double givenTimeLimit, bool viInFirstMILP, bool useCuts, bool useOldVI, bool branchingPriorityX, bool fixThreads, int numThreads);
	AlternatingDirectionMethod(const AlternatingDirectionMethod& alternatingDirectionMethod);
	~AlternatingDirectionMethod();

	//METHODS
	void solve();
	double availableComputationTime(bool inLoop);
	double requiredOptimalityGap(int iteration);

	//GET-SET ATTRIBUTES
	void writeStatistics(string fileNumber, string fileName);
	vector<unordered_map<int, double>> getBestX();
	double getBestObjectiveValue();

};
