#pragma once

#include "Common.h"

using namespace std;

class Season {

private:

	//ATTRIBUTES
	/* id, name, lower\upper bounds (one for each time instant)*/
	int id;
	string name;
	vector<int> timeInstants;

public:

	//CONSTRUCTORS AND DESTRUCTORS
	Season(int id, string name, vector<int> timeInstants);
	Season(const Season& season);
	~Season();

	//METHODS
	string toString();

	//GET-SET ATTRIBUTES
	int getId();
	string getName();
	vector<int>& getTimeInstants();

};


