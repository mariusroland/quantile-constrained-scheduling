#pragma once

#include "Common.h"
#include "Instance.h"
#include "MilpModel.h"
#include "AlternatingDirectionMethod.h"
#include <csignal>

using namespace std;

class AdaptiveAlgorithm {

private:

	//ATTRIBUTES

	//Algorithm parameters
	bool usingADM;
	bool useCuts;
	bool useOldVI;
	double givenTimeLimit = 0.0;
	double epsilonUBLB = 1.0;
	chrono::time_point<chrono::system_clock> startTime;

	//Data + Solve stuff
	Instance* instance;
	MilpModel* milpModel;

	//Solution
	vector<unordered_map<int, double>> bestXUBSolution;
	vector<unordered_map<int, double>> bestXLBSolution;
	vector<unordered_map<size_t, bool>> bestYUBSolution;
	vector<unordered_map<size_t, bool>> bestYLBSolution;
	vector<unordered_map<size_t, bool>> bestYSolution;
	double bestUB;
	double bestLB;

	//Solution study
	int ACPCounter;
	int MCPCounter;
	double adaptiveAlgorithmTime;

public:

	//CONSTRUCTORS AND DESTRUCTORS
	AdaptiveAlgorithm(Instance* instance,  double givenTimeLimit, double epsilonUBLB, bool usingADM, bool useCuts, bool useOldVI, bool branchingPriorityX, bool fixThreads, int numThreads);
	AdaptiveAlgorithm(const AdaptiveAlgorithm& adaptiveAlgorithm);
	~AdaptiveAlgorithm();

	//METHODS
	void solve();
	void warmstartProblem(bool kappa);
	bool solveModel(bool kappa);
	bool solveModelUsingADM(bool kappa);
	double availableComputationTime();
	double currentUBLBGap();
	double requiredOptimalityGap(int iteration);
	double requiredOptimalityGapADM(int iteration);
	double errorReclusteringPercentage(int iteration);
	void deepCopySolution(vector<unordered_map<int, double>>& initialVector, vector<unordered_map<int, double>>& endVector);
	double roundNumber(double value);
	void writeStatistics(string fileNumber, string fileName);


	//GET-SET ATTRIBUTES
	vector<unordered_map<int, double>> getBestXUB();
	vector<unordered_map<int, double>> getBestXLB();
	double getBestObjectiveValue();
};
