#include "ResourceConsumption.h"

///////////////////////CONSTRUCTORS AND DESTRUCTORS//////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

ResourceConsumption::ResourceConsumption(Resource* resource, int timeInstant, int startingTime, double consumption) {

	this->resource = resource;
	this->timeInstant = timeInstant;
	this->startingTime = startingTime;
	this->consumption = consumption;

}

ResourceConsumption::ResourceConsumption(const ResourceConsumption& resourceConsumption) {

	this->resource = resourceConsumption.resource;
	this->timeInstant = resourceConsumption.timeInstant;
	this->startingTime = resourceConsumption.startingTime;
	this->consumption = resourceConsumption.consumption;

}

ResourceConsumption::~ResourceConsumption() {

}

///////////////////////////METHODS//////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////toString////////////////////////////////////////////////////

string ResourceConsumption::toString() {

	string str;

	str.append("resource: ");
	str.append(resource->getName());

	str.append("\ttime instant: ");
	str.append(to_string(timeInstant));

	str.append("\t starting time: ");
	str.append(to_string(startingTime));

	str.append("\t consumption: ");
	str.append(to_string(consumption));

	return str;

}

////////////////////////////GET-SET ATTRIBUTES//////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////GET///////////////////////////////////////////////////

Resource* ResourceConsumption::getResource() {

	return this->resource;

}

int ResourceConsumption::getTimeInstant() {

	return this->timeInstant;

}

int ResourceConsumption::getStartingTime() {

	return this->startingTime;

}

double ResourceConsumption::getConsumption() {

	return this->consumption;

}