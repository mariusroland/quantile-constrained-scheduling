#pragma once

#include "Common.h"

using namespace std;

class Resource {

private:

	//ATTRIBUTES
	/* id, name, lower\upper bounds (one for each time instant)*/
	int id;
	string name;
	vector<double> lowerBounds;
	vector<double> upperBounds;

public:

	//CONSTRUCTORS AND DESTRUCTORS
	Resource(int id, string name, vector<double> lowerBounds, vector<double> upperBounds);
	Resource(const Resource& resource);
	~Resource();

	//METHODS
	string toString();

	//GET-SET ATTRIBUTES
	int getId();
	string getName();
	vector<double>& getLowerBounds();
	vector<double>& getUpperBounds();

};


