#!/bin/bash

methods=(1 2 3 4)
files=("A05" "B07" "B12" "B12" "X11" "X12")
inputPath="../../src/"
outputPath="./computation-output/"


for instanceFile in "${files[@]}"
do
    for methodCounter in "${methods[@]}"
    do
	sbatch single-run-25G.sh $inputPath $instanceFile $methodCounter $outputPath
    done
done



