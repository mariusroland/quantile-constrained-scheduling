#!/bin/bash
#SBATCH --cpus-per-task=5
#SBATCH --mem=15G
#SBATCH --time 120:00
#SBATCH --output=./logs/arrayjob_%A.out

dt1=`date '+%d/%m/%Y %H:%M:%S'`
echo "Current date: $dt1"
module load gurobi/9.5.0
../exec $1 $2 $3 $4
dt2=`date '+%d/%m/%Y %H:%M:%S'`
echo "Current date: $dt2"
echo "Finished: Running instances $start_file_num to $end_file_num with $num_procs processors"
wait
sleep 60
