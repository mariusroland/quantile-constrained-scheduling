This file gives the instructions to run the tests and what they do.

run_paper_tests.sh
=================
- use: simple bash script that runs the
executable "exec" a set of pre-selected instances
- function: goes through all the provided files and the selected setups
(inside 1-4)
- output: outputs the summary table data for each instance & method
- how to run: make executable with chmod then "./run_paper_tests.sh"
- warning: make sure that the exec file has all the correct parameters
before running

computation-output
==================
- use: folder that stores the summary table data and the solution
files coming from "run_paper_tests.sh"
- names: the files are name based on the instance that is solved and
the method that is used. For the respective solution the footer
"-solution" is added.
- milp data: file, optimality-gap, total-time-limit, read-time,
model-build-time, big-m-time, milp-solve-time, best-ub, best-lb, gap
- milp-vi data: file, optimality-gap, total-time-limit, read-time,
model-build-time, big-m-time, milp-solve-time, best-ub, best-lb, gap
- milp-vi-oadm data: file, adm-time-limit, adm-optimality-gap-y,
adm-optimality-gap-x, total-time-limit, read-time
model-build-time, big-m-time, ream-adm-time, improvement-solve-time,
best-ub-adm, best-ub, best-lb, gap
- milp-asca data: file, optimality-gap, adm-optimality-gap-y,
adm-optimality-gap-x, total-time-limit, read-time,
model-build-time, asca-solve-time, AC-iterations, MC-iterations,
best-ub, best-lb, gap

paper-data
==========
- use: folder that contains all the files used in the paper that are
produced using the python script
