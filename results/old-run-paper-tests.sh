#!/bin/bash
#SBATCH --cpus-per-task=4
#SBATCH --mem=90G
#SBATCH --time=180:00
#SBATCH --array=1-52
#SBATCH --output=./logs/arrayjob_%A_%a.out
#SBATCH --error=./logs/slurm_%A_%a.out

methods=(1 2 3 4)
files=("B14" "B15" "C12" "C13" "C14" "C15" "X05" "X06" "X07" "X08" "X13" "X14" "X15")
inputPath="../../src/"
outputPath="./computation-output/"

module load gurobi/9.5.0

i=1
for instanceFile in "${files[@]}"
do
    for methodCounter in "${methods[@]}"
    do
    if [ $SLURM_ARRAY_TASK_ID -eq $i ]
    then
	../exec $inputPath $instanceFile $methodCounter $outputPath
    fi
    sleep 150
    (( i = $i +1 ))
    done
done



