#!/bin/bash

inputPath="../../src/"
outputPath="./computation-output/"


sbatch single-run-90G.sh $inputPath "B15" 4 $outputPath
sbatch single-run-90G.sh $inputPath "C10" 3 $outputPath
sbatch single-run-90G.sh $inputPath "C12" 1 $outputPath


