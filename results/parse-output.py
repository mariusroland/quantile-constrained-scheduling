import pandas as pd
import numpy as np
import copy
import csv

#Define what has to be extracted
inFolder = "./computation-output/"
#"A02","A08","A11","A14","A15"
instanceClasses = {"A":["A02","A05","A08","A11","A14","A15"],"B":["B01","B02","B03","B04","B05","B06","B07","B08","B09","B10","B11","B12","B13","B14"],"C":["C01","C02","C03","C05","C06","C07","C08","C09","C11","C13","C14","C15"],"X":["X01","X02","X03","X05","X06","X07","X08","X09","X10","X11","X12","X13","X14","X15"]}
methods = ["milp","milp-vi","milp-vi-oadm","asca"]

#And define where the results have to be put
outFolder = "./paper-data/"

#Initializing data used for reading
rawData = {}
classTables = {}

# Read data
for instancesType in instanceClasses.keys():
    classTables[instancesType] = {}
    for instance in instanceClasses[instancesType]:
        for method in methods:
            rawData[instance,method] = pd.read_csv((inFolder+instance+"-"+method),sep=",",header=None)
        tableRow = [rawData[instance,"milp"][0][0],rawData[instance,"milp"][7][0],rawData[instance,"milp"][8][0],rawData[instance,"milp"][9][0],rawData[instance,"milp-vi"][7][0],rawData[instance,"milp-vi"][8][0],rawData[instance,"milp-vi"][9][0],rawData[instance,"milp-vi-oadm"][10][0],rawData[instance,"milp-vi-oadm"][11][0],rawData[instance,"milp-vi-oadm"][12][0],rawData[instance,"milp-vi-oadm"][13][0],rawData[instance,"asca"][10][0],rawData[instance,"asca"][11][0],rawData[instance,"asca"][12][0]]
        classTables[instancesType][instance] = tableRow

# Copy data and create bold entries
def returnBold(list1):

    #Copy first element of list and
    list2 = list(range(len(list1)))
    for i in range(len(list1)):
        if not isinstance(list1[i], str):
            list2[i] = "{:.2f}".format(list1[i])
        else:
            list2[i] = copy.copy(list1[i])

    #min UB
    placeUBs = [1,4,8,11]
    indexUB = placeUBs[0]
    minUB = list1[placeUBs[0]]
    for place in placeUBs[1:]:
        if minUB == "-" or (list1[place] != "-" and list1[place] < minUB):
            indexUB = place
            minUB = list1[place]
    list2[indexUB] = "\\textbf{"+"{:.2f}".format(list1[indexUB])+"}"

    #max LB
    placeLBs = [2,5,9,12]
    indexLB = placeLBs[0]
    maxLB = list1[placeLBs[0]]
    for place in placeLBs[1:]:
        if maxLB == "-" or (list1[place] != "-" and list1[place] > maxLB):
            indexLB = place
            maxLB = list1[place]
    list2[indexLB] = "\\textbf{"+"{:.2f}".format(list1[indexLB])+"}"

    #min Gap
    placeGaps = [3,6,10,13]
    indexGap = placeGaps[0]
    minGap = list1[placeGaps[0]]
    for place in placeGaps[1:]:
        if minGap == "-" or (list1[place] != "-" and list1[place] < minGap):
            indexGap = place
            minGap = list1[place]
    list2[indexGap] = "\\textbf{"+"{:.2f}".format(list1[indexGap])+"}"

    return list2

# def returnAverage(dict1):
#     placeGaps = [3,6,10,13]
#     averageGaps = []
#     for instance in dict1.keys():


#Apply the bold extraction to all tables
boldClassTables = copy.deepcopy(classTables)
for instancesType in instanceClasses.keys():
    for instance in instanceClasses[instancesType]:
        print(instance)
        boldClassTables[instancesType][instance] = returnBold(classTables[instancesType][instance])


# Print class tables to csv
for instanceClass in instanceClasses.keys():
    with open(outFolder+instanceClass+"-comparison.csv", "w", newline="\n") as f:
        writer = csv.writer(f)
        for instance in instanceClasses[instanceClass]:
            writer.writerow(boldClassTables[instanceClass][instance])

# Define fun to compare methods for bar plots
def compareMethods(instanceSubclasses, entries):
    list1 = []
    for instancesType in instanceSubclasses.keys():
        for instance in instanceSubclasses[instancesType]:
            list2 = list(range(len(entries)))
            for i in range(len(entries)):
                if classTables[instancesType][instance][entries[i]] != "-":
                    list2[i] = (classTables[instancesType][instance][entries[i]])/1000
                else:
                    list2[i] = ""
            list1.append(list2)
    return list1[::-1]

comparisonBarData = {}
#MIPvsVI
comparisonBarData["MIPvsVI"] = compareMethods(instanceClasses,[1,2,4,5])

#VIvsADM
instanceSubset = {"A":["A02","A08"],"B":["B06"]}
comparisonBarData["VIvsADM"]  = compareMethods(instanceSubset,[4,5,8,9])

#MIPvsASC
comparisonBarData["MIPvsASC"]  = compareMethods(instanceClasses,[1,2,11,12])

#ADMvsASC
comparisonBarData["ADMvsASC"]  = compareMethods(instanceClasses,[8,9,11,12])

# Print the data to the file with correct format
for comparison in comparisonBarData.keys():
    with open(outFolder+comparison+".txt", "w") as f:
        writer = csv.writer(f,delimiter="/",lineterminator=',\n')
        writer.writerows(comparisonBarData[comparison])

#Creating pandas table to apply summary data code
ABCX = pd.DataFrame()
individualDataframes = {}
for instanceClass in instanceClasses.keys():
    individualDataframes[instanceClass] = pd.DataFrame(classTables[instanceClass]).T
    print(individualDataframes[instanceClass])
    ABCX = pd.concat([ABCX,individualDataframes[instanceClass]])
ABCX.columns=["ID", "vUB","vLB","gap","vUB","vLB","gap","vUBOADM","vUB","vLB","gap","vUB","vLB","gap"]

#Replace dashes with NaN and remove ID and vUBOADM column
ABCX = ABCX.replace('-', np.nan)
ABCX = ABCX.drop(columns=["ID"])
ABCX = ABCX.drop(columns=["vUBOADM"])

#Create a table for each method
MILP = ABCX.iloc[:,range(0,3)]
MILPVIW = ABCX.iloc[:,range(3,6)]
MILPVIS = ABCX.iloc[:,range(6,9)]
ASCA = ABCX.iloc[:,range(9,12)]

# Create method table without dashes
ABCXna = ABCX.dropna()
MILPna = ABCXna.iloc[:,range(0,3)]
MILPVIWna = ABCXna.iloc[:,range(3,6)]
MILPVISna = ABCXna.iloc[:,range(6,9)]
ASCAna = ABCXna.iloc[:,range(9,12)]

#Computes the total improvement percentage for two columns
def getImpPerc(col1,col2):
    newcol = ((col1-col2)/col1*100).dropna()
    sumrows = newcol.sum()
    sizerows = len(newcol)
    return sumrows/sizerows

#Computes the total improvement quantity in terms of bounds found and not found
def getImpQty(col1,col2):
    newcol = (col1-col2).dropna()
    sumrows = newcol.sum()
    sizerows = len(newcol)
    return sumrows/sizerows

#Gets the number of NaN entries
def getNan(col):
    nacol = col.isna().sum()
    sizecol = col.shape[0]
    return sizecol-nacol

#Compares the amount of NaN entries between two columns
def getNanImp(col1,col2):
    nacol1 = col1.isna().sum()
    nacol2 = col2.isna().sum()
    return nacol1-nacol2

#Compares the improvement percentage for vUB, vLB, and gap between two methods
def getMethodComparison1(df1,df2):
    return [getImpPerc(df1["vUB"],df2["vUB"]),-getImpPerc(df1["vLB"],df2["vLB"]),getImpPerc(df1["gap"],df2["gap"])]

#Record the NaN entries for vUB and vLB
def getMethodComparison2(df):
    return [getNan(df["vUB"]),getNan(df["vLB"])]

#Save the statistics to file
comparisonStatistics1 = pd.DataFrame(data={"$\MILPviStr$":getMethodComparison1(MILPna,MILPVIWna), "$\MILPviStrOADM$":getMethodComparison1(MILPna,MILPVISna), "ASCA":getMethodComparison1(MILPna,ASCAna)}).T
comparisonStatistics1.to_csv("./paper-data/comparisonStatistics1.csv",header=False,float_format='%.2f')

comparisonStatistics2 = pd.DataFrame(data={"$\MILP$":getMethodComparison2(MILP),"$\MILPviStr$":getMethodComparison2(MILPVIW), "$\MILPviStrOADM$":getMethodComparison2(MILPVIS), "ASCA":getMethodComparison2(ASCA)}).T
comparisonStatistics2.to_csv("./paper-data/comparisonStatistics2.csv",header=False,float_format='%.2f')

def computeAverage(df):
    dfNaGap1 = df.replace('-', np.nan).dropna()
    dfNaGap1 = dfNaGap1.drop(columns=0)
    sizecol = dfNaGap1.shape[1]
    print(sizecol)
    dfNaGap1 = dfNaGap1.sum(axis=0)/sizecol
    print(dfNaGap1)
    for col in range(1,sizecol):
        if col not in [3,6,10,13]:
            dfNaGap1[col]=np.nan
    return dfNaGap1

def compute_average(dataframe_given):

    dataframe = dataframe_given.replace('-', np.nan).dropna()
    dataframe = dataframe.drop(columns=0)
    print(dataframe)

    # Compute the average of all entries in the input DataFrame
    average = dataframe.mean()
    print(average)

    # Create a new DataFrame with one row representing the average
    average_dataframe = pd.DataFrame([average], columns=dataframe.columns)
    print(average_dataframe)

    # Keep only the columns with indices 3, 6, 10, and 13
    indices_to_keep = [3, 6, 10, 13]

    # Replace average values of other columns with NaN
    average_dataframe = average_dataframe.reindex(dataframe.columns, axis=1)
    average_dataframe.loc[:, average_dataframe.columns.difference(indices_to_keep)] = np.nan

    return average_dataframe

for instanceType in individualDataframes.keys():
    averageDf = compute_average(individualDataframes[instanceType]).T
    averageDf.T.to_csv("./paper-data/"+instanceType+"-averages.csv",header=False,index=False,float_format='%.2f')
