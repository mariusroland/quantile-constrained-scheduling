#!/bin/bash

methods=(1 2 3 4)
files=("B14" "B15" "C12" "C13" "C14" "C15" "X05" "X06" "X07" "X08" "X13" "X14" "X15")
inputPath="../../src/"
outputPath="./computation-output/"


for instanceFile in "${files[@]}"
do
    for methodCounter in "${methods[@]}"
    do
	sbatch single-run-90G.sh $inputPath $instanceFile $methodCounter $outputPath
    done
done



