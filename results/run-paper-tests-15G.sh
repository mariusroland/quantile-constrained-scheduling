#!/bin/bash

methods=(1 2 3 4)
files=("X01" "X02" "X03" "X04" "X09" "X10" "C01" "C02" "C03" "C04" "C05" "C06" "C07" "C08" "C09" "C10" "C11" "B01" "B02" "B03" "B04" "B05" "B06" "B07" "B08" "B09" "B10" "B11" "B13" "A02" "A08" "A11" "A14" "A15")
#files=("A08")
inputPath="../../src/"
outputPath="./computation-output/"


for instanceFile in "${files[@]}"
do
    for methodCounter in "${methods[@]}"
    do
	sbatch single-run-15G.sh $inputPath $instanceFile $methodCounter $outputPath
    done
done



