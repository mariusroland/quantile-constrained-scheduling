#include "Intervention.h"

///////////////////////CONSTRUCTORS AND DESTRUCTORS//////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

Intervention::Intervention(int id, string name, int maximumTime, vector<double> processingTimes/*, vector<ResourceConsumption> resourceConsumptions, vector<Risk> risks*/) {

	this->id = id;
	this->name = name;
	this->maximumTime = maximumTime;
	this->processingTimes = processingTimes;

}

Intervention::Intervention(const Intervention& intervention) {

	this->id = intervention.id;
	this->name = intervention.name;
	this->maximumTime = intervention.maximumTime;
	this->processingTimes = intervention.processingTimes;

}

Intervention::~Intervention() {

}

///////////////////////////METHODS//////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////toString////////////////////////////////////////////////////

string Intervention::toString() {

	string str;

	str.append("id: ");
	str.append(to_string(id));

	str.append("\tname: ");
	str.append(name);

	str.append("\t max time: ");
	str.append(to_string(maximumTime));

	str.append("\t processing time: ");
	str.append(to_string(processingTimes.size()));

	return str;

}

////////////////////////////GET-SET ATTRIBUTES//////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////GET///////////////////////////////////////////////////

int Intervention::getId() {

	return this->id;

}

string Intervention::getName() {

	return this->name;

}

int Intervention::getMaximumTime() {

	return this->maximumTime;

}

vector<int>& Intervention::getFeasibleStartingTimes() {

	return this->feasibleStartingTimes;

}

vector<double>& Intervention::getProcessingTimes() {

	return this->processingTimes;

}

vector<vector<int>>& Intervention::getInProcess() {

	return this->inProcess;

}