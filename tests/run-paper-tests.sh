#!/bin/bash

methods=(3 4)
files=("X15" "X06" "A05" "A15" "A08")
inputPath="../../src/"
outputPath="../results/computation-output/"
logPath="./logs/"

for instanceFile in "${files[@]}"
do
    for methodCounter in "${methods[@]}"
    do
	/usr/bin/time -o $logPath$instanceFile"-"$methodCounter"-memory.log" -v ../exec $inputPath $instanceFile $methodCounter $outputPath > $logPath$instanceFile"-"$methodCounter".log" & 
    done
done
