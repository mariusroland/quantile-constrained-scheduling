CCC = g++
EXEC = exec
FLAGS =
LIBFLAGS = -I$(GRBPATH)linux64/include -L$(GRBPATH)linux64/lib -lgurobi_c++ -lgurobi95 -fopenmp
GRBPATH = /opt/gurobi950/
SRC = $(wildcard *.cpp)
OBJ = $(SRC:.cpp=.o)

all:	$(EXEC)

%.o: %.cpp
	$(CCC) $(FLAGS) -o $@ -c $< $(LIBFLAGS)

$(EXEC): $(OBJ)
	$(CCC) $(FLAGS) -o $@ $^ $(LIBFLAGS)

clean:
	rm -f $(EXEC) *.o *~
