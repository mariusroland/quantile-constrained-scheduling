#include "AdaptiveAlgorithm.h"

///////////////////////CONSTRUCTORS AND DESTRUCTORS//////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

AdaptiveAlgorithm::AdaptiveAlgorithm(Instance* instance, double givenTimeLimit, double epsilonUBLB, bool usingADM, bool useCuts, bool useOldVI, bool branchingPriorityX, bool fixThreads, int numThreads) {

	//Data and model
	this->instance = instance;
	this->milpModel = new MilpModel(this->instance, false, false, false, false, branchingPriorityX, fixThreads, numThreads);

	//Algorithm parameters
	this->startTime = this->instance->getStartTimeInitialization();
	this->givenTimeLimit = givenTimeLimit;
	this->usingADM = usingADM;
	this->useCuts = useCuts;
	this->useOldVI = useOldVI;
	this->epsilonUBLB = epsilonUBLB;

	//Solution variables
	this->bestXUBSolution = vector<unordered_map<int, double>> (this->instance->getInterventions().size());
	this->bestXLBSolution = vector<unordered_map<int, double>> (this->instance->getInterventions().size());
	this->bestYUBSolution = vector<unordered_map<size_t, bool>> (this->instance->getTimeHorizon(),unordered_map<size_t, bool> {});
	this->bestYLBSolution = vector<unordered_map<size_t, bool>> (this->instance->getTimeHorizon(),unordered_map<size_t, bool> {});
	this->bestUB = maxConstant;
	this->bestLB = 0.0;

	//Solution study
	this->ACPCounter = 0;
	this->MCPCounter = 0;
	this->adaptiveAlgorithmTime = 0.0;
}

AdaptiveAlgorithm::AdaptiveAlgorithm(const AdaptiveAlgorithm& adaptiveAlgorithm) {

}

AdaptiveAlgorithm::~AdaptiveAlgorithm() {
	delete this->milpModel;
}

///////////////////////////METHODS//////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

void AdaptiveAlgorithm::solve(){

	cout << endl << "STARTING ADAPTIVE ALGORITHM" << endl << endl;

	//Algorithm parameters
	vector<unordered_map<int, double>> currentXSolution (this->instance->getInterventions().size());
	double currentObjectiveValue;
	bool kappa = true;
	bool problemChange = false;
	bool hadImprovementPhase = false;
	int iteration = 1;

	if (this->availableComputationTime() > 0){
		//Initial problem preparation without quantile
		this->milpModel->initializeFirstMILP_Adaptive(this->usingADM, kappa);
		//this->milpModel->writeModel("test.lp");
	}

	//Computational information
	chrono::time_point<chrono::system_clock> start = chrono::system_clock::now();

	if (this->availableComputationTime() > 0){
		cout << endl << "FIRST ACP ITERATION" << endl << endl;
		this->milpModel->solveModel(this->availableComputationTime(),this->requiredOptimalityGap(iteration));
		this->ACPCounter++;

		this->milpModel->getXValues(currentXSolution);
		this->deepCopySolution(currentXSolution,this->bestXUBSolution);
		this->deepCopySolution(currentXSolution,this->bestXLBSolution);
		this->bestUB = milpModel->objectiveValueRecovery(this->bestXUBSolution);
		this->bestYSolution = vector<unordered_map<size_t, bool>> (this->instance->getTimeHorizon(),unordered_map<size_t, bool> {});
	}

	cout<<"CURRENT GAP: "<< this->currentUBLBGap() << endl;

	while (this->currentUBLBGap() > this->epsilonUBLB && this->availableComputationTime() > 0.0){


		this->milpModel->computeClusterRisk_adaptive(currentXSolution);
		this->milpModel->computeErrorEstimators_adaptive();
		// this->milpModel->writeModel("test.lp");
		// cout<<endl<<"hello boyz"<<endl;
		// raise(SIGINT);
		this->milpModel->computeImportantTimesteps_adaptive(errorReclusteringPercentage(iteration));
		this->milpModel->initializeNextMILP_adaptive(kappa,problemChange);

		if (kappa){
			cout << endl << "ACP ITERATION" << endl << endl;
			this->ACPCounter++;
			hadImprovementPhase = this->solveModel(kappa);
			this->milpModel->getXValues(currentXSolution);
			currentObjectiveValue = this->milpModel->objectiveValueRecovery(currentXSolution);
			if (this->bestUB > currentObjectiveValue){
				this->deepCopySolution(currentXSolution,this->bestXUBSolution);
				this->bestUB = currentObjectiveValue;
				problemChange = false;
			}
			else{
				kappa = false;
				problemChange = true;
			}
		}
		else{
			cout << endl << "MCP ITERATION" << endl << endl;
			this->MCPCounter++;
			hadImprovementPhase = this->solveModel(kappa);
			if (hadImprovementPhase){
				this->milpModel->getXValues(currentXSolution);
				this->milpModel->extractSolutionAttributes();
				currentObjectiveValue = this->milpModel->getModelObjectiveBound();
				if (this->bestLB < currentObjectiveValue){
					this->deepCopySolution(currentXSolution,this->bestXLBSolution);
					this->bestLB = currentObjectiveValue;
					problemChange = false;
				}
				else{
					kappa = true;
					problemChange = true;
				}
			}
			currentObjectiveValue = this->milpModel->objectiveValueRecovery(currentXSolution);
			if (this->bestUB > currentObjectiveValue){
				this->deepCopySolution(currentXSolution,this->bestXUBSolution);
				this->bestUB = currentObjectiveValue;
			}

		}

		cout<<"CURRENT GAP: "<< this->currentUBLBGap() << endl;

	}

	//Saving computational information
	chrono::time_point<chrono::system_clock> end = chrono::system_clock::now();
	this->adaptiveAlgorithmTime = (chrono::duration_cast<chrono::nanoseconds>	(end - start).count())*1e-9;

}


void AdaptiveAlgorithm::warmstartProblem(bool kappa){

	bool newX = true;

	if (kappa){
		this->milpModel->computeWarmStartYWithX(this->bestXUBSolution,this->bestYSolution,newX);
		this->milpModel->warmStartYVariable(this->bestYSolution);
		this->milpModel->warmStartXVariable(this->bestXUBSolution);
	}
	else {
		this->milpModel->computeWarmStartYWithX(this->bestXLBSolution,this->bestYSolution,newX);
		this->milpModel->warmStartYVariable(this->bestYSolution);
		this->milpModel->warmStartXVariable(this->bestXLBSolution);
	}
}

bool AdaptiveAlgorithm::solveModel(bool kappa){

	bool hadImprovementPhase = true;
	if (this->usingADM){
		hadImprovementPhase = this->solveModelUsingADM(kappa);
	}
	else{
		this->warmstartProblem(kappa);
		if (this->useCuts){
			this->milpModel->initializeCutCallback(this->useOldVI,true);
		}
		this->milpModel->solveModel(this->availableComputationTime(),this->requiredOptimalityGap(1));
		this->milpModel->removeCallback();
	}
	return hadImprovementPhase;
}


bool AdaptiveAlgorithm::solveModelUsingADM(bool kappa){

	cout << endl << "STARTING ADM" << endl << endl;

	//Best solution saving
	vector<unordered_map<int, double>> xValues(this->instance->getInterventions().size());
	if (kappa){
		this->deepCopySolution(this->bestXUBSolution,xValues);
	}
	else {
		this->deepCopySolution(this->bestXLBSolution,xValues);
	}
	vector<unordered_map<size_t, bool>> yValues(this->instance->getTimeHorizon(),unordered_map<size_t, bool> {});
	double currentBestObjectiveValue = maxConstant;
	double newObjectiveValue;

	int admIterationCounter = 1;
	bool isEnd = false;
	bool endedInXSolving = true;

	//One iteration per variable type
	while (!isEnd && this->availableComputationTime() > 0) {

		//If the iteration count is uneven
		//Do y solving
		if (admIterationCounter % 2) {
				cout << endl << "Y SOLVING" << endl;

				//Remove y cons and add x fixing
				this->milpModel->ySolving_ADM_Adaptive(xValues,yValues,newObjectiveValue);

				//Saving in X or Y solving
				endedInXSolving = false;

			}

			//Otherwise, if the iteration count is even
			//Do x solving
			else {
				cout << endl << "X SOLVING" << endl;

				// //Remove x cons and add y fixing
				this->milpModel->warmStartYVariable(yValues);
				this->milpModel->warmStartXVariable(xValues);
				this->milpModel->addConstraints_yVariableFixing(yValues);

				//Solve with reference param and time limit
				this->milpModel->solveModel(this->availableComputationTime(),this->requiredOptimalityGapADM(admIterationCounter));

				//Get objective value and xValues
				this->milpModel->extractSolutionAttributes();
				newObjectiveValue = this->milpModel->getModelObjectiveValue();

				//Remove remaining constraints
				this->milpModel->removeConstraints_yVariableFixing();

				//Saving in X or Y solving
				endedInXSolving = true;

			}

		//If the solution has been improved
		if (newObjectiveValue < currentBestObjectiveValue*0.999999999) {

			if (endedInXSolving){
				this->milpModel->getXValues(xValues);
			}

			currentBestObjectiveValue = newObjectiveValue;


		}

		//If solution is not improved
		else if (newObjectiveValue >= currentBestObjectiveValue*0.9999999999) {

			isEnd = true;

		}

		admIterationCounter++;

	}

	if (this->availableComputationTime() > 0) {
		cout << endl << "IMPROVEMENT PHASE" << endl << endl;
		if (this->useCuts){
			this->milpModel->initializeCutCallback(this->useOldVI,true);
		}
		//Warm starting
		if (endedInXSolving){
			this->milpModel->getXValues(xValues);
		}
		this->milpModel->warmStartXVariable(xValues);
		this->milpModel->warmStartYVariable(yValues);
		this->milpModel->solveModel(this->availableComputationTime(),this->requiredOptimalityGap(1));
		this->milpModel->removeCallback();
		return true;
	}
	else {
		return false;
	}
}


double AdaptiveAlgorithm::availableComputationTime(){

	chrono::time_point<chrono::system_clock> currentTime = chrono::system_clock::now();
	return (this->givenTimeLimit  - (chrono::duration_cast<chrono::nanoseconds>	(currentTime - this->startTime).count())*1e-9 - this->milpModel->getMaxObjectiveValueRecoveryTime());

}

double AdaptiveAlgorithm::currentUBLBGap(){

	return (this->bestUB - this->bestLB)/this->bestUB;
}

double AdaptiveAlgorithm::requiredOptimalityGap(int iteration){

	return 2.5e-2;

}

double AdaptiveAlgorithm::requiredOptimalityGapADM(int iteration){

	if (iteration % 2) {
		// Y solving
		return 0.0;
	}
	else{
		// X solving
		return 2.5e-3;
	}

}

double AdaptiveAlgorithm::errorReclusteringPercentage(int iteration){

	return 0.25;

}

void AdaptiveAlgorithm::deepCopySolution(vector<unordered_map<int, double>>& initialVector, vector<unordered_map<int, double>>& endVector){

	for (int t = 0; t < this->instance->getTimeHorizon(); t = t + 1) {

		for (int j = 0; j < this->instance->getScenarioNumber().at(t); j = j + 1) {

			for (int k = 0; k < this->instance->getInterventions().size(); k++) {

				Intervention* intervention = &this->instance->getInterventions().at(k);

				for (int s = 0; s < intervention->getInProcess().at(t).size(); s++) {

					int startingTime = intervention->getInProcess().at(t).at(s);

					endVector[intervention->getId()][startingTime] = initialVector[intervention->getId()][startingTime];


				}

			}
		}
	}

}

double AdaptiveAlgorithm::roundNumber(double value){
  return round(value * 100.0)/100.0;
}

void AdaptiveAlgorithm::writeStatistics(string fileNumber, string fileName){

	//Printing solution if available
	if (this->bestUB < maxConstant){
		this->milpModel->computeFullYWithX(this->bestXUBSolution,this->bestYUBSolution);
		this->milpModel->solutionPrinting(this->bestXUBSolution,this->bestYUBSolution,fileName+"-solution-ub");
	}
	if (this->bestLB > 0.0){
		this->milpModel->computeFullYWithX(this->bestXLBSolution,this->bestYLBSolution);
		this->milpModel->solutionPrinting(this->bestXLBSolution,this->bestYLBSolution,fileName+"-solution-lb");
	}


	bool UbandLBAvailable = true;

	ofstream mystream;
	mystream.open(fileName, ofstream::out | ofstream::trunc);
	//Computation data
	mystream << fileNumber << ",";
	mystream << this->requiredOptimalityGap(0) << ",";
	mystream << this->requiredOptimalityGapADM(1) << ",";
	mystream << this->requiredOptimalityGapADM(2) << ",";
	mystream << this->roundNumber(this->givenTimeLimit) << ",";

	mystream << fixed << setprecision(2);
	mystream << this->instance->getReadTime() << ",";
	mystream << this->milpModel->getModelBuildTime() << ",";
	mystream << this->adaptiveAlgorithmTime << ",";
	mystream << this->ACPCounter << ",";
	mystream << this->MCPCounter << ",";
	if (this->bestUB < maxConstant){
		mystream << this->bestUB << ",";
	}
	else{
		mystream << "-" << ",";
		UbandLBAvailable = false;
	}
	if (this->bestLB > 0.0){
		mystream << this->bestLB << ",";
	}
	else{
		mystream << "-" << ",";
		UbandLBAvailable = false;
	}
	if (UbandLBAvailable){
		mystream << (this->currentUBLBGap()*100.0);
	}
	else{
		mystream << "-";
	}
	//Solution data
	mystream << "\n";
	mystream.close();

}


////////////////////////////GET-SET ATTRIBUTES//////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////GET///////////////////////////////////////////////////

vector<unordered_map<int, double>> AdaptiveAlgorithm::getBestXUB(){
	return this->bestXUBSolution;
}

vector<unordered_map<int, double>> AdaptiveAlgorithm::getBestXLB(){
	return this->bestXLBSolution;
}

double AdaptiveAlgorithm::getBestObjectiveValue(){
	return this->bestUB;
}
