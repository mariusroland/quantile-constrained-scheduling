#pragma once

#include "Common.h"
#include "Resource.h"

using namespace std;

class ResourceConsumption {

private:

	//ATTRIBUTES
	/* resource, time instant, starting time, consumption*/
	Resource* resource;
	int timeInstant;
	int startingTime;
	double consumption;

public:

	//CONSTRUCTORS AND DESTRUCTORS
	ResourceConsumption(Resource* resource, int timeInstant, int startingTime, double consumption);
	ResourceConsumption(const ResourceConsumption& resourceConsumption);
	~ResourceConsumption();

	//METHODS
	string toString();

	//GET-SET ATTRIBUTES
	Resource* getResource();
	int getTimeInstant();
	int getStartingTime();
	double getConsumption();

};


