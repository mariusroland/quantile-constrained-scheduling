#pragma once

#include "Common.h"
#include "Intervention.h"
#include "Season.h"

using namespace std;

class Exclusion {

private:

	//ATTRIBUTES
	/* id, name, intervention1, intervention2, season*/
	int id;
	string name;
	Intervention* intervention1;
	Intervention* intervention2;
	Season* season;

public:

	//CONSTRUCTORS AND DESTRUCTORS
	Exclusion(int id, string name, Intervention* intervention1, Intervention* intervention2, Season* season);
	Exclusion(const Exclusion& exclusion);
	~Exclusion();

	//METHODS
	string toString();

	//GET-SET ATTRIBUTES
	int getId();
	string getName();
	Intervention* getIntervention1();
	Intervention* getIntervention2();
	Season* getSeason();

};


